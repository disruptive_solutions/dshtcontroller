////////////////////////////////////////
// I2C Custom
////////////////////////////////////////
#ifndef _I2CDS_H_
#define _I2CDS_H_

/* I2C variables */
//palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);   /* SCL */
#define stm32F411SclPort1       GPIOB
#define stm32F411SclPin1           6
//palSetPadMode(GPIOB, 7, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);   /* SDA */
#define stm32F411SdaPort1        GPIOB
#define stm32F411SdaPin1           7
#define stm32F411SdaPort2        GPIOB
#define stm32F411SdaPin2         GPIOB_PIN2

#define stm32McuConfI2CD         I2CD1

/* sensor addresses */
//#define chipcapI2CAddr 0x28 //Not yet sure about this!!

/* I2C interface #1 */
static const I2CConfig i2cfg1 = {
    OPMODE_I2C, //Specifies the I2C mode. op_mode
    100000, //clock_speed 100000 for begin and 400000 for fast
    FAST_DUTY_CYCLE_2, //STD_DUTY_CYCLE, //FAST_DUTY_CYCLE_2, //Specifies the I2C fast mode duty cycle.
};

void initI2CDs(BaseSequentialStream *chp);
bool i2cBusReset(BaseSequentialStream *chp);
void cmd_diic(BaseSequentialStream *chp, int argc, char *argv[]);
void cmd_piic(BaseSequentialStream *chp, int argc, char *argv[]);
bool powerUpI2CDs(bool powerChipCap2);

#endif  /* _I2CDS_H_ */
