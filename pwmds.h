////////////////////////////////////////
// PWM Custom
////////////////////////////////////////
#ifndef _PWMDS_H_
#define _PWMDS_H_

/* PWM variables */
#define stm32F411PwmPort1          GPIOA
#define stm32F411PwmPin1           8

/*
 * PWM configuration structure.
 * Cyclic callback enabled, channels 1 and 4 enabled without callbacks,
 * the active state is a logic one.
 */
static const PWMConfig pwmcfg = {
  1000000, /* 1MHz PWM clock frequency */
  20000, /* PWM period 20 milli  second */
  NULL,  /* No callback */
  {
        {PWM_OUTPUT_ACTIVE_HIGH, NULL},
        {PWM_OUTPUT_ACTIVE_HIGH, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
        {PWM_OUTPUT_DISABLED, NULL},
  },
  /* HW dependent part.*/
  0,
  0
};

void initPwmDs(BaseSequentialStream *chp);

#endif  /* _PWMDS_H_ */
