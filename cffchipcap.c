#include <stdlib.h>
#include <stdio.h>
#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "i2cds.h"
#include <cffchipcap.h>

static volatile msg_t i2cStatus = MSG_OK;
static i2cflags_t i2cErrors = 0;
static uint8_t wakeUpCmd[chipcapBufferCmdDepth]; //msg_t??
static uint8_t data[chipcapBufferDataDepth];
static systime_t tmo = MS2ST(4);
static uint8_t sensorValues[2];
static unsigned humidityValue;
static unsigned temperatureValue;

////////////////////////////////////////
// ChipCap2 Digital I2C Class Methods
////////////////////////////////////////

//
// // Configure which pin is used for the ChipCap2 Ready pin and set as input
// void CFF_ChipCap2::configReadyPin(uint8_t readypin)
// {
//     _readypin = readypin;
//     pinMode(_readypin, INPUT);
// }
//
// // Configure which pin is used for the ChipCap2 Alarm Low pin and set as input
// void CFF_ChipCap2::configAlarmLowPin(uint8_t alowpin)
// {
//     _alowpin = alowpin;
//     pinMode(_alowpin, INPUT);
// }
//
// // Configure which pin is used for the ChipCap2 Alarm High pin and set as input
// void CFF_ChipCap2::configAlarmHighPin(uint8_t ahighpin)
// {
//     _ahighpin = ahighpin;
//     pinMode(_ahighpin, INPUT);
// }
//
// // If using the pwrpin for powering chipcap use this function to turn it on or off
// void CFF_ChipCap2::power(uint8_t onOff)
// {
//     if(_pwrpin != NULL)
//     {
//         if(onOff == 0)
//             digitalWrite(_pwrpin, LOW);
//
//         if(onOff == 1)
//             digitalWrite(_pwrpin, HIGH);
//     }
// }
//
// // Check the ChipCap2 Ready pin to see if conversion is complete
// bool CFF_ChipCap2::dataReady(void)
// {
//     uint8_t val;
//
//     val = digitalRead(_readypin);
//     if ( val == 0)
//         return false;
//     if (val == 1)
//         return true;
//
// }
//
// // Check for Alarm Low Trigger
// bool CFF_ChipCap2::checkAlarmLow(void)
// {
//     uint8_t val;
//
//     val = digitalRead(_alowpin);
//     if ( val == 0)
//         return false;
//     if (val == 1)
//         return true;
// }
//
// // Set Alarm Low Trigger Value in Celsius
// void CFF_ChipCap2::setAlarmLowVal(int16_t alarmlowon)
// {
//     readSensor();
//     if (status == CCF_CHIPCAP2_STATUS_COMMANDMODE)
//     {
//         uint16_t alowon, alowoff;
//         uint8_t lowByte, hiByte;
//         int8_t alarmlowoff = alarmlowon + 1;
//         alowon = ((alarmlowon + 40) * 99);
//         lowByte = alowon & 255;
//         hiByte = alowon >> 8;
//         Wire.beginTransmission(_i2caddr);
//         Wire.write(0x5A);
//         Wire.write(hiByte);
//         Wire.write(lowByte);
//         Wire.endTransmission();
//         delay(2);
//         alowoff = ((alarmlowoff + 40) * 99);
//         lowByte = alowoff & 255;
//         hiByte = alowoff >> 8;
//         Wire.beginTransmission(_i2caddr);
//         Wire.write(0x5B);
//         Wire.write(hiByte);
//         Wire.write(lowByte);
//         Wire.endTransmission();
//         delay(2);
//     }
// }
//
// int16_t CFF_ChipCap2::getAlarmLowVal()
// {
//     readSensor();
//     if (status == CCF_CHIPCAP2_STATUS_COMMANDMODE)
//     {
//         uint8_t edat[3];
//         int16_t eALow;
//         Wire.beginTransmission(_i2caddr);
//         Wire.write(0x1A);
//         Wire.write(0);
//         Wire.write(0);
//         Wire.endTransmission();
//         delay(1);
//         Wire.beginTransmission(_i2caddr);
//         Wire.requestFrom((uint8_t)_i2caddr, (uint8_t)3, (uint8_t)true);
//         edat[0] = Wire.read();
//         edat[1] = Wire.read();
//         edat[2] = Wire.read();
//         Wire.endTransmission();
//         eALow = (((edat[1] << 8) + (edat[2])) / 99) - 40;
//         return eALow;
//     }
// }
//
// // Check for Alarm High Trigger
// bool CFF_ChipCap2::checkAlarmHigh(void)
// {
//     uint8_t val;
//
//     val = digitalRead(_ahighpin);
//     if ( val == 0)
//         return false;
//     if (val == 1)
//         return true;
// }
//
// // Set Alarm High Trigger Value in Celsius
// void CFF_ChipCap2::setAlarmHighVal(int16_t alarmhighon)
// {
//     readSensor();
//     if (status == CCF_CHIPCAP2_STATUS_COMMANDMODE)
//     {
//         uint16_t ahion, ahioff;
//         uint8_t lowByte, hiByte;
//         int8_t alarmhighoff = alarmhighon - 1;
//         ahion = ((alarmhighon + 40) * 99);
//         lowByte = ahion & 255;
//         hiByte = ahion >> 8;
//         Wire.beginTransmission(_i2caddr);
//         Wire.write(0x58);
//         Wire.write(hiByte);
//         Wire.write(lowByte);
//         Wire.endTransmission();
//         delay(2);
//         ahioff = ((alarmhighoff + 40) * 99);
//         lowByte = ahioff & 255;
//         hiByte = ahioff >> 8;
//         Wire.beginTransmission(_i2caddr);
//         Wire.write(0x59);
//         Wire.write(hiByte);
//         Wire.write(lowByte);
//         Wire.endTransmission();
//         delay(2);
//     }
// }
//
// int16_t CFF_ChipCap2::getAlarmHighVal()
// {
//     readSensor();
//     if (status == CCF_CHIPCAP2_STATUS_COMMANDMODE)
//     {
//         uint8_t edat[3];
//         int16_t eAHigh;
//         Wire.beginTransmission(_i2caddr);
//         Wire.write(0x18);
//         Wire.write(0);
//         Wire.write(0);
//         Wire.endTransmission();
//         delay(1);
//         Wire.beginTransmission(_i2caddr);
//         Wire.requestFrom((uint8_t)_i2caddr, (uint8_t)3, (uint8_t)true);
//         edat[0] = Wire.read();
//         edat[1] = Wire.read();
//         edat[2] = Wire.read();
//         Wire.endTransmission();
//         eAHigh = (((edat[1] << 8) + (edat[2])) / 99.29) - 40;
//         return eAHigh;
//     }
// }
//
// Enters command mode (Only valid during first 10ms after power-on)
void startCommandMode(BaseSequentialStream *chp)
{
    // if (_pwrpin != NULL)
    // {
    //     power(0);
    //     delay(50);
    //     power(1);
    //     delay(2);
    // }
    wakeUpCmd[0] = 0xA0; //8 bits = 1 byte = 0b10000000
    wakeUpCmd[1] = 0x00; //8 bits = 1 byte
    wakeUpCmd[2] = 0x00; //8 bits = 1 byte
    i2cAcquireBus(&stm32McuConfI2CD);
    i2cStatus = i2cMasterTransmitTimeout(&stm32McuConfI2CD, CFF_CHIPCAP2_DEFAULT_ADDR, wakeUpCmd, sizeof(wakeUpCmd), data, sizeof(data), tmo);
    if (i2cStatus != MSG_OK){
      i2cErrors = i2cGetErrors(&stm32McuConfI2CD);
        //chprintf(chp, "I2C NOT OK! * startCommandMode\r\n");
    }
    else
    {
       palTogglePad(GPIOA, GPIOA_LED_GREEN);
         //chprintf(chp, "I2C OK! * startCommandMode\r\n");
    }
    chThdSleepMilliseconds(5);
    i2cReleaseBus(&stm32McuConfI2CD);
    //chThdSleepMilliseconds(5);
}
//
// Enters normal mode of operation
void startNormalMode(BaseSequentialStream *chp)
{
    uint8_t wakeUpCmd[chipcapBufferCmdDepth] = {0x80,0,0};
    wakeUpCmd[0] = 0x80; //8 bits = 1 byte = 0b10000000
    //wakeUpCmd[1] = 0x; //8 bits = 1 byte
    //wakeUpCmd[2] = 0x00; //8 bits = 1 byte
    i2cAcquireBus(&stm32McuConfI2CD);
    i2cStatus = i2cMasterTransmitTimeout(&stm32McuConfI2CD, CFF_CHIPCAP2_DEFAULT_ADDR, wakeUpCmd, sizeof(wakeUpCmd), data, sizeof(data), tmo);
    //chprintf(chp, "I2C STATUS:\r\n");
    //chprintf(chp, i2cStatus);
    //chprintf(chp, "\r\n");
    if (i2cStatus != MSG_OK){
      i2cErrors = i2cGetErrors(&stm32McuConfI2CD);
      //chprintf(chp, "I2C ERROR:\r\n");
      //chprintf(chp, i2cErrors);
      //chprintf(chp, "\r\n");
        //chprintf(chp, "I2C NOT OK! * startNormalMode\r\n");
    }
    else
    {
       palTogglePad(GPIOA, GPIOA_LED_GREEN);
         //chprintf(chp, "I2C OK * startNormalMode!\r\n");
    }
    chThdSleepMilliseconds(5);
    i2cReleaseBus(&stm32McuConfI2CD);
}
//
// // Read Humidity and Temperature Sensor Data
void readSensor(BaseSequentialStream *chp)
{
    i2cAcquireBus(&stm32McuConfI2CD);
    chThdSleepMilliseconds(5);
    i2cMasterTransmit(&stm32McuConfI2CD, chipcapI2CAddr, data, 1, NULL, 0);
    i2cMasterReceive(&stm32McuConfI2CD, chipcapI2CAddr, data, 1);
    //i2cMasterReceiveTimeout(&stm32McuConfI2CD, chipcapI2CAddr,data,sizeof(data),tmo);
    //chprintf(chp, "Hier kom ik langs 1\r\n");
    //chThdSleepMilliseconds(5);
    i2cReleaseBus(&stm32McuConfI2CD);
    //chThdSleepMilliseconds(5);
    //chprintf(chp, "Reading I2C OK * readSensor!");
    sensorValues[0] = data[0];
    //humidityValue = (((data[0] & 63) << 8) + data[1]) / 163.84;
    //sensorValues[0] = humidityValue;
    //chprintf(chp, strcpy("",humidityValue));
    //temperatureValue = (((data[2] << 6) + (data[3] / 4)) / 99.29) - 40;
    //sensorValues[1] = temperatureValue;
    //chprintf(chp, "Hier kom ik langs\r\n");
    //cprintf("%s", temperatureValue);

}

uint8_t getHumidityValue(void) {
   /* get the current humidityvalue */
   return sensorValues[0];
}

uint8_t getTemperatureValue(void) {
   /* get the current humidityvalue */
   return sensorValues[1];
}
