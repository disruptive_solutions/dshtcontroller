/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Added new functionality based on the ChibiOS demo. Ben alias DisruptiveNL

    Clean Code principles used:
    - Use Intention-Revealing Names

    An I2C file to address all I2C definitions and functionality at one place

    (c) 2017 DisruptiveNL
    For: NUCLEO-STMF411RE
*/
#include <stdlib.h>
#include <stdbool.h>
#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include <i2cds.h>

// Initiate I2C bus
void initI2CDs(BaseSequentialStream *chp)
{
    //I2C -- Page 45,46 and 48
    //palSetPadMode(stm32F411SdaPort1, stm32F411SdaPin1, PAL_STM32_MODE_ALTERNATE | PAL_STM32_OTYPE_OPENDRAIN | PAL_STM32_OSPEED_HIGHEST | PAL_STM32_ALTERNATE(4));
    //palSetPadMode(stm32F411SclPort1, stm32F411SclPin1, PAL_STM32_MODE_ALTERNATE | PAL_STM32_OTYPE_OPENDRAIN | PAL_STM32_OSPEED_HIGHEST | PAL_STM32_ALTERNATE(4));

    //palSetPadMode(stm32F411SdaPort1, stm32F411SdaPin1, PAL_STM32_OTYPE_OPENDRAIN);
    //palSetPadMode(stm32F411SclPort1, stm32F411SclPin1, PAL_STM32_OTYPE_OPENDRAIN);
    //palSetPadMode(stm32F411SdaPort1, stm32F411SdaPin1, PAL_MODE_ALTERNATE(4));
    //palSetPadMode(stm32F411SclPort1, stm32F411SclPin1, PAL_MODE_ALTERNATE(4));
    //palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(4));   /* SCL */
    //  palSetPadMode(GPIOB, 7, PAL_MODE_ALTERNATE(4));   /* SDA */
    palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);   /* SCL */
    palSetPadMode(GPIOB, 7, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);   /* SDA */
    /*
    * Starting the I2C driver 2.
    * http://www.i2c-bus.org/i2c-primer/how-i2c-hardware-works/
    */
    i2cStart(&stm32McuConfI2CD, &i2cfg1);
    chprintf(chp, "I2C started!\r\n");
}

bool powerUpI2CDs(bool powerChipCap2)
{
  if (!powerChipCap2)
  {
    palClearPad(stm32F411SdaPort2, stm32F411SdaPin2);
    chThdSleepMilliseconds(500);
    palSetPad(stm32F411SdaPort2, stm32F411SdaPin2);
    return true;
  }
  return false;
}

/* Dump I2C */
/* count = how many bytes you want to read */
void cmd_diic(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint8_t addr;
    uint8_t regaddr;
    int count, i;
    uint8_t rxBuf[16] = {0};
    msg_t ret = MSG_OK;

    if (argc != 3)
    {
        chprintf(chp, "diic deviceaddress regaddress count\r\n");
        return;
    }

    addr = strtol(argv[0], NULL, 16);
    regaddr = strtol(argv[1], NULL, 16);
    count = strtol(argv[2], NULL, 10);

    if (count > 16)
        count = 16;

    i2cAcquireBus(&stm32McuConfI2CD);
    for (i=0; i<count; i++)
    {
        i2cMasterTransmit(&stm32McuConfI2CD, addr, (uint8_t[]){regaddr+i}, 1, NULL, 0);
        ret |= i2cMasterReceive(&stm32McuConfI2CD, addr, rxBuf+i, 1);
    }
    i2cReleaseBus(&stm32McuConfI2CD);

    if (ret != MSG_OK)
    {
        //chprintf(chp, "[%02x] Error\n\r");
        chprintf(chp, "Error %d\n\r", ret);
        return;
    }

    chprintf(chp, "[%02x] %02x:", addr, regaddr);
    for (i=0 ; i<count; i++)
        chprintf(chp, " %02x", rxBuf[i]);
    chprintf(chp, "\n\r");
}

void cmd_piic(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint8_t addr;
    uint8_t txBuf[2];
    msg_t ret;

    if (argc != 3)
    {
        chprintf(chp, "piic deviceaddress regaddress data\r\n");
        return;
    }

    addr = strtol(argv[0], NULL, 16);
    txBuf[0] = strtol(argv[1], NULL, 16);
    txBuf[1] = strtol(argv[2], NULL, 16);

    i2cAcquireBus(&stm32McuConfI2CD);
    ret = i2cMasterTransmit(&stm32McuConfI2CD, addr, txBuf, 2, NULL, 0);
    i2cReleaseBus(&stm32McuConfI2CD);

    if (ret != MSG_OK)
    {
        chprintf(chp, "[%02x] Error\n\r");
        return;
    }

    chprintf(chp, "[%02x] %02x: %02x\n\rvoid", addr, txBuf[0], txBuf[1]);
}

/*
 * I2C Bus Reset - Analog Devices AN-686 Solution 1.
 */

bool i2cBusReset(BaseSequentialStream *chp)
{
    int count = 0;
    bool ret = false;

    /* Configure I2C pins to GPIO */
    palSetPadMode(stm32F411SclPort1, stm32F411SclPin1, PAL_STM32_MODE_OUTPUT | PAL_STM32_OTYPE_OPENDRAIN | PAL_STM32_OSPEED_HIGHEST);
    palSetPadMode(stm32F411SdaPort1, stm32F411SdaPin1, PAL_STM32_MODE_OUTPUT | PAL_STM32_OTYPE_OPENDRAIN | PAL_STM32_OSPEED_HIGHEST);
    chThdSleepMilliseconds(1);

    /* Try to make signals high */
    palSetPad(stm32F411SclPort1, stm32F411SclPin1);
    palSetPad(stm32F411SdaPort1, stm32F411SdaPin1);
    chThdSleepMilliseconds(1);

    /* If SDA is low, clock SCL until SDA is high */
    while (palReadPad(stm32F411SdaPort1, stm32F411SdaPin1) == PAL_LOW && count < 50)
    {
        palClearPad(stm32F411SclPort1, stm32F411SclPin1);
        chThdSleepMilliseconds(1);
        palSetPad(stm32F411SclPort1, stm32F411SclPin1);
        chThdSleepMilliseconds(1);
        count++;
    }

    /* Generate a stop condition */
    palClearPad(stm32F411SclPort1, stm32F411SclPin1);
    chThdSleepMilliseconds(1);
    palClearPad(stm32F411SdaPort1, stm32F411SdaPin1);
    chThdSleepMilliseconds(1);
    palSetPad(stm32F411SclPort1, stm32F411SclPin1);
    chThdSleepMilliseconds(1);
    palSetPad(stm32F411SdaPort1, stm32F411SdaPin1);
    chThdSleepMilliseconds(1);

    /* Return true, if SDA is now high */
    ret = (palReadPad(stm32F411SdaPort1, stm32F411SdaPin1) == PAL_HIGH);

    palSetPadMode(stm32F411SclPort1, stm32F411SclPin1, PAL_STM32_MODE_ALTERNATE | PAL_STM32_OTYPE_OPENDRAIN | PAL_STM32_OSPEED_HIGHEST | PAL_STM32_ALTERNATE(4));
    palSetPadMode(stm32F411SdaPort1, stm32F411SdaPin1, PAL_STM32_MODE_ALTERNATE | PAL_STM32_OTYPE_OPENDRAIN | PAL_STM32_OSPEED_HIGHEST | PAL_STM32_ALTERNATE(4));

    if (count > 0)
        chprintf(chp, " - I2C was stuck, and took %d cycles to reset.\n\r");

    chThdSleepMilliseconds(10);

    return ret;
}
