////////////////////////////////////////
// ChipCap2 Digital I2C Class
////////////////////////////////////////
#ifndef _CFFCHIPCAP2_H_
#define _CFFCHIPCAP2_H_

#define CFF_CHIPCAP2_DEFAULT_ADDR 0x28
#define CCF_CHIPCAP2_STATUS_VALID 0
#define CCF_CHIPCAP2_STATUS_STALE 1
#define CCF_CHgiveBackInt()IPCAP2_STATUS_COMMANDMODE 2
#define CCF_CHIPCAP2_COMMAND_BUSY 0
#define CCF_CHIPCAP2_COMMAND_SUCCESS 1
#define CCF_CHIPCAP2_COMMAND_ERROR 2
#define CCF_CHIPCAP2_EEPROM_ERROR_CORRECTED 2
#define CCF_CHIPCAP2_EEPROM_ERROR_UNCORRECTED 4
#define CCF_CHIPCAP2_EEPROM_ERROR_RAM 8
#define CCF_CHIPCAP2_EEPROM_ERROR_CONFIG 16
#define CCF_CHIPCAP2_READY_PIN 2
#define CCF_CHIPCAP2_ALARMLOW_PIN 3
#define CCF_CHIPCAP2_ALARMHIGH_PIN 4

/* buffers depth */
#define chipcapBufferDataDepth 6 //Array depth
#define chipcapBufferCmdDepth  3 //Array depth
#define chipcapI2CAddr 0x28

void begin(void);
void startCommandMode(BaseSequentialStream *chp);
void startNormalMode(BaseSequentialStream *chp);
void readSensor(BaseSequentialStream *chp);
//function declaration isn't a prototype -- add void!!
uint8_t getHumidityValue(void);
uint8_t getTemperatureValue(void);
// void cmd_measureA(BaseSequentialStream *chp, int argc, char *argv[]);
// void cmd_measureDirect(BaseSequentialStream *chp, int argc, char *argv[]);
// void cmd_Vref(BaseSequentialStream *chp, int argc, char *argv[]);
// void cmd_Temperature(BaseSequentialStream *chp, int argc, char *argv[]);
//
// void cmd_measureCont(BaseSequentialStream *chp, int argc, char *argv[]);
// void cmd_measureRead(BaseSequentialStream *chp, int argc, char *argv[]);
// void cmd_measureStop(BaseSequentialStream *chp, int argc, char *argv[]);
//
// void myADCinit(void);

#endif  /* _CFFCHIPCAP2_H_ */
