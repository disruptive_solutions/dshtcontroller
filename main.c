/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Added new functionality based on the ChibiOS demo. Ben alias DisruptiveNL

    Clean Code principles used:
    - Use Intention-Revealing Names

    Used FAN: AFC1212D (PWM) -- Note: when you power the FAN with its own +12V
                                connect the GND from the controller and external power
*/
#include <stdlib.h>
#include <stdbool.h>
#include "ch.h"
#include "hal.h"
#include "pwm.h"
#include "chprintf.h"
#include "shell.h"
#include "test.h"
#include "i2cds.h"
#include "pwmds.h"
#include "cffchipcap.h"

/* Shell variables */
//The scope of #define is limited to the file in which it is defined.
#define TEST_WA_SIZE    THD_WORKING_AREA_SIZE(256)
#define SHELL_WA_SIZE   THD_WORKING_AREA_SIZE(2048)

/* buffers depth */
#define chipcapBufferDataDepth 6 //Array depth
#define chipcapBufferCmdDepth  3 //Array depth

/* sensor addresses */
#define chipcapI2CAddr 0x28 //Not yet sure about this!!

/* defining arrays */
static BaseSequentialStream * chp = (BaseSequentialStream*) &SD2;
//static uint8_t data[chipcapBufferDataDepth];

/* I2C variables */
// static i2cflags_t i2cErrors = 0; //NOT static when in a seperate h-file!!!!
// static volatile msg_t i2cStatus = MSG_OK;

/* defining values variables */
//static int32_t humidityValue;
//static float temperatureValue;
static volatile msg_t genericDebug = 0;

static bool powerChipCap2 = false;

static float humidityValue;
static float temperatureValue;
static float temperatureF;

/* testing */
uint8_t txbuf[3]; //was 2
uint8_t rxbuf[4];

/* I2C interface #1 */
static const I2CConfig i2cfgtest = {
    OPMODE_I2C, //Specifies the I2C mode. op_mode
    400000, //clock_speed 100000 for begin and 400000 for fast
    FAST_DUTY_CYCLE_2, //STD_DUTY_CYCLE, //FAST_DUTY_CYCLE_2, //Specifies the I2C fast mode duty cycle.
};

/*===========================================================================*/
/* Command line related.                                                     */
/*===========================================================================*/

void initShellDs(BaseSequentialStream *chp)
{
  //Documentation for making shell work -- Chapter 6.8 manual NUCLEO
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7)); /* USART1 TX. */
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7)); /* USART1 RX. */

  /*
   * Shell manager initialization.
   * You dont need to go SD over USB. The nucleo has one of uart ports routed to the stlink ~kimmoli
   */
  shellInit();
  chprintf(chp, "Shell init");
}

static void cmd_mem(BaseSequentialStream *chp, int argc, char *argv[]) {
  size_t n, size;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: mem\r\n");
    return;
  }
  n = chHeapStatus(NULL, &size);
  chprintf(chp, "core free memory : %u bytes\r\n", chCoreGetStatusX());
  chprintf(chp, "heap fragments   : %u\r\n", n);
  chprintf(chp, "heap free total  : %u bytes\r\n", size);
}

static void cmd_threads(BaseSequentialStream *chp, int argc, char *argv[]) {
  static const char *states[] = {CH_STATE_NAMES};
  thread_t *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: threads\r\n");
    return;
  }
  chprintf(chp, "    addr    stack prio refs     state time\r\n");
  tp = chRegFirstThread();
  do {
    chprintf(chp, "%08lx %08lx %4lu %4lu %9s\r\n",
            (uint32_t)tp, (uint32_t)tp->p_ctx.r13,
            (uint32_t)tp->p_prio, (uint32_t)(tp->p_refs - 1),
            states[tp->p_state]);
    tp = chRegNextThread(tp);
  } while (tp != NULL);
}

static void cmd_test(BaseSequentialStream *chp, int argc, char *argv[]) {
  thread_t *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: test\r\n");
    return;
  }
  tp = chThdCreateFromHeap(NULL, TEST_WA_SIZE, chThdGetPriorityX(),
                           TestThread, chp);
  if (tp == NULL) {
    chprintf(chp, "out of memory\r\n");
    return;
  }
  chThdWait(tp);
}

static const ShellCommand commands[] = {
  {"mem", cmd_mem},
  {"threads", cmd_threads},
  {"test", cmd_test},
  {"diic", cmd_diic}, //See i2ccustom
  {"piic", cmd_piic}, //See i2ccustom
  {NULL, NULL}
};

/*
* Shell configuration for the serial protocol and port
*/

static const ShellConfig shell_cfg1 = {
  (BaseSequentialStream *)&SD2, // --> WHEN USING USART2
  //(BaseSequentialStream *)&SDU1,
  commands
};

/*
 * Red LED blinker thread, times are in milliseconds. -- Heartbeat
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
  }
}

/*
 * I2C thread for reading the sensor (preperation)
 */
static THD_WORKING_AREA(myThreadWorkingArea, 128);
static THD_FUNCTION(myThread, arg) {
  (void)arg;

}

 /*
  * Application entry point.
  */

 int main(void) {

   thread_t *shelltp = NULL;

   //MS2ST(msec) - Milliseconds to system ticks. -> http://chibios.sourceforge.net/html/group__time.html
   //systime_t tmo = MS2ST(4);

   /*
    * System initializations.
    * - HAL initialization, this also initializes the configured device drivers
    *   and performs the board-specific initializations.
    * - Kernel initialization, the main() function becomes a thread and the
    *   RTOS is active.
    */
   halInit();
   chSysInit();

   txbuf[0] = 0x00;
   txbuf[0] = 0x00;

   initShellDs(chp);

   initPwmDs(chp);

   //initI2CDs(chp);
   //powerChipCap2 = powerUpI2CDs(powerChipCap2);
   //startCommandMode(chp);
   //chThdSleepMilliseconds(6000);
   //startCommandMode(chp);
   //startNormalMode(chp);

   /*
    * Creates the blinker thread.
    */
   chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
   genericDebug = 8;

   palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);   /* SCL */
   palSetPadMode(GPIOB, 7, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);   /* SDA */
   //palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(4));   /* SCL */
  // palSetPadMode(GPIOB, 7, PAL_MODE_ALTERNATE(4));   /* SDA */
   /*
   * Starting the I2C driver 2.
   * http://www.i2c-bus.org/i2c-primer/how-i2c-hardware-works/
   */
   i2cStart(&stm32McuConfI2CD, &i2cfgtest);

   //static const uint8_t cmd2[] = {0xA0,0,0}; //go to Normal mode
   static const uint8_t cmd2[] = {0x80,0,0}; //go to Normal mode
   i2cAcquireBus(&I2CD1);
   i2cMasterTransmit(&I2CD1, 0x28, cmd2, sizeof(cmd2), rxbuf, 4);
   i2cMasterReceive(&I2CD1, 0x28, rxbuf, 4);
   i2cReleaseBus(&I2CD1);
   chThdSleepMilliseconds(1000);
  //  i2cAcquireBus(&I2CD1);
  //  i2cMasterTransmit(&I2CD1, 0x28, txbuf, sizeof(txbuf), rxbuf, 4);
  //  i2cReleaseBus(&I2CD1);

   chprintf(chp, "I2C started!\r\n");

   /*
   * Creates the I2C thread.
   */

   //chThdCreateStatic(myThreadWorkingArea, sizeof(myThreadWorkingArea),
   //                        NORMALPRIO, myThread, NULL);
   //i2cBusReset(chp);
   /* main loop */
   while (1) {
                /* Shell initialization */
                if (!shelltp) {
                   //if (SDU1.config->usbp->state == USB_ACTIVE) {
                     /* Spawns a new shell.*/
                     shelltp = shellCreate(&shell_cfg1, SHELL_WA_SIZE, NORMALPRIO);
                   //}
                 }
                 else {
                   /* If the previous shell exited.*/
                   if (chThdTerminatedX(shelltp)) {
                     /* Recovers memory of the previous shell.*/
                     chThdRelease(shelltp);
                     shelltp = NULL;
                   }
                 }
                /* http://chibios.sourceforge.net/html/group___p_w_m.html */
                /* dutycycle is used to control PWM and FAN */
                pwmEnableChannel(&PWMD1, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD1, 2500));   // 25% duty cycle

                /* http://chibios.sourceforge.net/html/group__threads.html */
                //chThdSleepMilliseconds(2000);
                //static const uint8_t cmd3[] = {0x56,0x5F,0}; //go to Normal mode
                i2cAcquireBus(&I2CD1);
                i2cMasterReceive(&I2CD1, 0x28, rxbuf, 4);
                //i2cReleaseBus(&I2CD1);
                chThdSleepMilliseconds(1000);
                //i2cMasterReceive(&I2CD1, 0x28, rxbuf, 4);
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf, sizeof(txbuf), rxbuf, 4);
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf, sizeof(txbuf), rxbuf, 4);


                //chThdSleepMilliseconds(5);
                //txbuf[0] = 0x80;
                //txbuf[1] = 0x00;
                //txbuf[2] = 0x00;
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf[0], 1, rxbuf, 2);
                //chThdSleepMilliseconds(5);
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf[1], 1, rxbuf, 2);
                //chThdSleepMilliseconds(5);
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf[2], 1, rxbuf, 2);
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf[1], 1, rxbuf, 2);
                //i2cMasterTransmit(&I2CD1, 0x28, txbuf[2], 1, rxbuf, 2);
                //chThdSleepNanoseconds(5);
                //i2cMasterTransmit(&I2CD1, 0x80, txbuf, 1, rxbuf, 2);
                //chThdSleepMilliseconds(5);
                //i2cMasterTransmit(&I2CD1, 0x16, txbuf, 1, rxbuf, 2);
                //i2cReleaseBus(&I2CD1);
                temperatureValue = (((rxbuf[2] << 6) + (rxbuf[3] / 4)) / 99.29) - 40;
                humidityValue = (((rxbuf[0] & 63) << 8) + rxbuf[1]) / 163.84;
                temperatureF = temperatureValue * 1.8 + 32;
                //chprintf(chp,"Temp F: %d \r\n",temperatureValue);
                ///chprintf(chp, "Temp F: %d \r\n",temperatureF);
                //chprintf(chp, "Hum: %d \r\n",humidityValue);
                //chprintf(chp, "Received: 0x%02X 0x%02X 0x%02X 0x%02X\r\n", rxbuf[0], rxbuf[1],rxbuf[2], rxbuf[3]);


                //i2cAcquireBus(&I2CD1);
                i2cMasterTransmit(&I2CD1, 0x28, txbuf, sizeof(txbuf), rxbuf, 4);
                i2cReleaseBus(&I2CD1);

                chThdSleepMilliseconds(500);
                //startNormalMode(chp);
                //chThdSleepMilliseconds(2000);
                //chThdSleepMilliseconds(5000);
                //startNormalMode(chp);
                //readSensor(chp);
                //humidityValue = getHumidityValue();
                //temperatureValue = getTemperatureValue();

                 /* print the actual value */
                //chprintf(chp, "Waarde:\r\n");
                //chprintf(chp, "\r\n");
                //chprintf(chp, humidityValue);

                //
                /* Some calculation try-outs */
                //humidity = (((rxbuf[0] & 63) << 8) + rxbuf[1]) / 163.84;
                // temperatureC = (((rxbuf[2] << 6) + (rxbuf[3] / 4)) / 99.29) - 40;
                // temperatureF = temperatureC * 1.8 + 32;

                // humidityValue = (((data[0] & 63) << 8) + data[1]) / 163.84;
                // temperatureValue = (((data[2] << 6) + (data[3] / 4)) / 99.29) - 40;

   }

   return 0;
}
