/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Added new functionality based on the ChibiOS demo. Ben alias DisruptiveNL

    Clean Code principles used:
    - Use Intention-Revealing Names

    An I2C file to address all I2C definitions and functionality at one place

    (c) 2017 DisruptiveNL
    For: NUCLEO-STMF411RE
*/
#include <stdlib.h>
#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include <pwmds.h>

void initPwmDs(BaseSequentialStream *chp)
{
    //PWM
    palSetPadMode(stm32F411PwmPort1, stm32F411PwmPin1, PAL_MODE_ALTERNATE(1)); //Page 47 Datasheet STM32F411RE

    /*
    * Starting the PWM driver 1.
    */
    pwmStart(&PWMD1, &pwmcfg); //TIMER 1!! PWMD9 = TIM9
    chprintf(chp, "PWM started!");
}
