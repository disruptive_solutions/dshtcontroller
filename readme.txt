*****************************************************************************
** ChibiOS/RT port for ARM-Cortex-M4 STM32F411.                            **
*****************************************************************************

License: 
http://www.binpress.com/license/view/l/629d55c1c79d71ee2bd2d59be88669ff

** TARGET **

The software runs on an ST_NUCLEO_F411RE board.

** The Project **

The software flashes the board LED using a thread. And a Fan is controlled by the PWM protocol. Also is a ChipCap2 sensor read with I2C and this input is used to intervent the PWM signal. Based on temp and/or humidity treshold.

** Build Procedure **

Gdb and St-Util
* sudo arm-none-eabi-gdb build/ch.elf
* st-util
* make clean and make

** Notes **

Thanks also to the IRQ channel ChibiOS!!

Some files used by the demo are not part of ChibiOS/RT but are copyright of
ST Microelectronics and are licensed under a different license.
Also note that not all the files present in the ST library are distributed
with ChibiOS/RT, you can find the whole library on the ST web site:

                             http://www.st.com